﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
//Rubén Plaza Garcia
namespace p70_GestionDeDatos
{
    class Program
    {
        
        static int cont;
        
        //creo una lista de animales , para ir metiendo cada animal ahi , gracias al polimorfismo.
        static List<Animal> listaAnimales = new List<Animal>();
        public static int dia, mes, anyo;
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            TipoAnimal.leeTipos();
            StreamReader sr = new StreamReader(@"..\..\Datos\zoo.txt");
            string frase = "";
            string[] tabCampos;
            string[] fecha;
            string[] fechaIns;
            
             
            while (!sr.EndOfStream)
            {
                
                frase = sr.ReadLine();
               
                tabCampos = frase.Split(';');               
                fecha = tabCampos[4].Split('/');
               
                fechaIns = tabCampos[5].Split('/');
                dia = Convert.ToInt32(fecha[0]);
                mes = Convert.ToInt32(fecha[1]);
                anyo = Convert.ToInt32(fecha[2]);

                listaAnimales.Add(new Animal(Convert.ToInt32( tabCampos[0]),Convert.ToInt32( tabCampos[1]),tabCampos[2],tabCampos[3],new Fecha(anyo,mes,dia),new Fecha(Convert.ToInt32( fechaIns[2]), Convert.ToInt32(fechaIns[1]), Convert.ToInt32(fechaIns[0]))));

                
               
                
            }
            sr.Close();
           
            cont = listaAnimales.Count + 1;//igualo el tamaño de la lista al contador y lse sumo 1 para que no se repita 
            int opcion;
            do
            {

                opcion = Util.Menu();
                Console.Clear();
                switch (opcion)
                {
                    //aqui muestro TODOS los animales
                    case 1:
                       
                        CabezeraGeneral();
                        
                        foreach (Animal item in listaAnimales)
                        {
                            item.Mostrar();
                            
                        }
                        
                        break;
                    case 2:
                        //aqui pido los datos para introducir nuevos animales
                        /*Tu método DatosAnimal()  debería pedir todos los datos, no sólo nombre y sexo, y devolver un animal, no una lista. Y debes usarlo en añadir  y en modificar*/
                        Console.Beep();

                        listaAnimales.Add(DatosAnimal(false));

                        //y le sumo 1 al contador de los id 
                        cont++;
                         Console.WriteLine("\n\t\tAñadido , pulse tecla para continuar");
                               
                        
                        break;
                    case 3:
                        //pido el animal a modificar, a traves del metodo BuscaAnimal que devuelve su posicion y si no la encontrado devuelve -1
                        for (int i = 0; i < listaAnimales.Count; i++)
                        {
                            Console.WriteLine("\t\t{0}) {1}",listaAnimales[i].Id,listaAnimales[i].Nombre);
                        }
                        int id = BuscaAnimal("Introduzca el id del animal a modificar");
                        if (id == -1)
                            Console.WriteLine("\n\tEse animal no existe",14);
                        else
                        {
                            //si lo ha encontrado pido los datos para modificarlos
                            //meto en esta lista una llamada a Datos animal, que devuelve una lista con el nombre y el sexo
                            Console.Clear();
                            CabezeraGeneral();
                            Console.WriteLine("{0} {1}{2}{3}{4}{5}\n", listaAnimales[id].Id.ToString("000"), Util.CuadraTexto(TipoAnimal.listaDeTipos[listaAnimales[id].IdTipoAnimal].NombreTipo, 12), Util.CuadraTexto(listaAnimales[id].Nombre, 10), Util.CuadraTexto(listaAnimales[id].Sexo, 9), Util.CuadraTexto(listaAnimales[id].FechaNac.FechaString, 30), listaAnimales[id].FechaInscripcion.FechaStringSp);
                            Animal animalMod = DatosAnimal(true);
                            if(animalMod.Nombre!=string.Empty)
                            listaAnimales[id].Nombre = animalMod.Nombre;
                            if (animalMod.IdTipoAnimal >= 0)
                                listaAnimales[id].IdTipoAnimal = animalMod.IdTipoAnimal;
                            if(animalMod.Sexo!="")
                                listaAnimales[id].Sexo = animalMod.Sexo;
                            if(animalMod.FechaNac.Anyo>=0)
                            listaAnimales[id].FechaNac= animalMod.FechaNac;
                            Console.Write("\n\t\tCambio registrado , pulse tecla para volver al menu");
                        }
                        break;
                    case 4:
                        //aqui se vuelve hacer una llamada para buscar al animal selecionado, para luego poder eliminarlo
                        for (int i = 0; i < listaAnimales.Count; i++)
                        {
                            Console.WriteLine("\t\t{0}) {1}", listaAnimales[i].Id, listaAnimales[i].Nombre);
                        }
                        id = BuscaAnimal("Introduzca el id del animal a eliminar");
                        if (id == -1)
                            Console.WriteLine("\n\tEse animal no existe", 14);
                        else
                        {
                            //si lo ha encontrado, elimino su posicion
                            listaAnimales.RemoveAt(id);
                            Console.Write("\n\t\tAnimal eliminado , pulse tecla para volver al menu");
                        }

                        break;
                       
                    case 5:
                        //pido un tipo de animal para filtrar la busqueda por ese tipo
                        TipoAnimal.Mostrar();
                       int opcionFiltrar = Util.CapturaEntero("\n\n\tSeleciona una opción: ", 1, TipoAnimal.listaDeTipos.Count) - 1;
                        CabezeraGeneral();
                        for (int i = 0; i < listaAnimales.Count; i++)
                        {
                            if (listaAnimales[i].IdTipoAnimal == opcionFiltrar)
                                listaAnimales[i].Mostrar();
                            
                        }

                        break;
                    case 6:
                        
                        int opcionTipos = -1 ;
                        
                        while (opcionTipos != 0)
                        {
                            opcionTipos = Util.MenuTipos();
                            Console.Clear();
                            switch (opcionTipos)
                            {

                                case 1:
                                    Console.WriteLine("\n\t\tTipos de animales");
                                    Console.WriteLine("\n\t\t--------------------");
                                    TipoAnimal.Mostrar();
                                    break;

                                case 2:
                                    string nombreTipo = Util.CapturaNombre("Dime el nombre del tipo de animal a añadir", 15);
                                    TipoAnimal.listaDeTipos.Add(new TipoAnimal(nombreTipo));
                                    Console.WriteLine("\n\t\tTipo añadido");
                                    break;
                                case 3:
                                    TipoAnimal.Mostrar();
                                    int posicion = Util.CapturaEntero("Dime el tipo a modificar") - 1;
                                    nombreTipo = Util.CapturaNombre("Dime el nombre del tipo de animal a modificar", 15);
                                    TipoAnimal.listaDeTipos[posicion].NombreTipo = nombreTipo;
                                    Console.WriteLine("\n\t\tTipo modificado");
                                    break;
                                case 0: 
                                    break;
                                default:Console.WriteLine("\n\t\tOpcion no valida");
                                    break;
                                   
                            }
                            if (opcionTipos != 0)
                                Console.ReadKey();
                        }
                        break;
                    case 0:                                             
                        break;
                    default:
                        Console.WriteLine("\n\tOpcion no valida",14);
                        break;
                }
                if(opcion!=0 && opcion!=6)
                Console.ReadKey();
            } while (opcion != 0);

            GuardarCambios();//cuando el usuario decide salir, hago una llamada a GuardarCambios

        }
        public static void GuardarCambios() {

            char respuesta = Util.PreguntaSiNo("Quieres guardar los cambios?(S=si o N=no)");
            if (respuesta == 's' || respuesta == 'S')
            {
                //si decide guardar cambios hago lo siguiente:
                StreamWriter sw = new StreamWriter(@"..\..\Datos\zoo.txt");

                for (int i = 0; i < listaAnimales.Count; i++)
                {
                    sw.WriteLine(listaAnimales[i].Id + ";" + listaAnimales[i].IdTipoAnimal + ";" + listaAnimales[i].Nombre + ";" + listaAnimales[i].Sexo + ";" + listaAnimales[i].FechaNac.FechaStringSp + ";" + listaAnimales[i].FechaInscripcion.FechaStringSp);
                }
                sw.Close();

                 sw = new StreamWriter(@"..\..\Datos\Tipos.txt");

                for (int i = 0; i < TipoAnimal.listaDeTipos.Count; i++)
                {
                    sw.WriteLine(TipoAnimal.listaDeTipos[i].NombreTipo);
                }
                sw.Close();
            }
           
        }
        

        public static int BuscaAnimal(string texto)
        {


            int id = Util.CapturaEntero("\n\t\t" + texto);
            //busca el animal y si lo encuentra devuelve su posicion en la lista
            for (int i = 0; i < listaAnimales.Count; i++)
            {
                if (listaAnimales[i].Id == id)
                    return i;
            }
            return -1;

        }

        public static Animal DatosAnimal(bool modificar)
        {
            int idTipo = 0;
            string sexo = string.Empty;
            bool esCorrecto = false;
            string nombre;
            TipoAnimal.Mostrar();
            if(!modificar)
             idTipo=Util.CapturaEntero("Dime el tipo",0,TipoAnimal.listaDeTipos.Count)-1;
            else
              idTipo = Util.CapturaEnteroModificar("Dime el tipo (INTRO para siguiente)", 0, TipoAnimal.listaDeTipos.Count) - 1;
            if (modificar)
            {
                Console.Write("\n\tDime el nombre(INTRO para siguiente): ");
                 nombre = Console.ReadLine();
            }
            else
                nombre=Util.CapturaNombre("Dime el nombre",15);
            
            
           
            
            do
            {
                esCorrecto = false;
                if (modificar)
                {
                    Console.Write("\n\tDime el sexo (M=Macho H=Hembra)(INTRO para siguiente): ");
                    char tecla = Console.ReadKey().KeyChar;
                    if (tecla !=13)
                    {
                        if (tecla == 'M' || tecla == 'm')
                        {
                            sexo = "Macho";
                            esCorrecto = true;
                        }
                        else if (tecla == 'H' || tecla == 'h')
                        {
                            sexo = "Hembra";
                            esCorrecto = true;
                        }
                        else
                        {
                            Console.WriteLine("\n\tPulse M o H", 14);
                            Console.ReadKey();
                        }
                    }
                    else
                    {
                        esCorrecto = true;
                        sexo = "";
                    }
                }
                else {
                    Console.Write("\n\tDime el sexo (M=Macho H=Hembra): ");
                    char tecla = Console.ReadKey().KeyChar;
                  
                    
                        if (tecla == 'M' || tecla == 'm')
                        {
                            sexo = "Macho";
                            esCorrecto = true;
                        }
                        else if (tecla == 'H' || tecla == 'h')
                        {
                            sexo = "Hembra";
                            esCorrecto = true;
                        }
                        else
                        {
                        Console.WriteLine("\n\tPulse M o H", 14);
                            Console.ReadKey();
                        }
                    
                }
                
            } while (!esCorrecto);

            if (!modificar)
            {
                int[] maxDiaMes = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
                anyo = Util.CapturaEntero("\n\tDime el año de nacimiento: ", 1950, 2020);
                mes = Util.CapturaEntero("\tDime el mes de nacimiento: ", 1, 12);
                if (mes == 2 && ((anyo % 4 == 0 && anyo % 100 != 0) || anyo % 400 == 0))
                    maxDiaMes[mes] = 29;
                dia = Util.CapturaEntero("\tDime el día de nacimiento: ", 1, maxDiaMes[mes]);
            }
            else {
                int[] maxDiaMes = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
                anyo = Util.CapturaEnteroModificar("\n\tDime el año de nacimiento(Intro para Terminar): ", 1950, 2020);
                if (anyo != -1)
                {
                    mes = Util.CapturaEntero("\tDime el mes de nacimiento: ", 1, 12);
                    if (mes == 2 && ((anyo % 4 == 0 && anyo % 100 != 0) || anyo % 400 == 0))
                        maxDiaMes[mes] = 29;
                    dia = Util.CapturaEntero("\tDime el día de nacimiento: ", 1, maxDiaMes[mes]);
                }
               
            }

            return new Animal(cont,idTipo,nombre,sexo,new Fecha(anyo,mes,dia));

        }
        public static  void CabezeraGeneral()
        {
            //esta es la cabezera general para el mostrar todos los elementos
            Console.WriteLine("\nID   TIPO       NOMBRE    SEXO    FECHA NACIMIENTO         FECHA INSCRIPCION");
            Console.WriteLine("-----------------------------------------------------------------------------");
            

        }
    }
}
