﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace p70_GestionDeDatos
{
    class TipoAnimal
    {
        /*Lo adecuado sería tener una clase TipoAnimal con campos idTipo y nombreTipo y construir una lista de tipos 
         * (si prefieres que el idTipo se corresponda con su posición en la lista no hace falta el campo idTipo dentro de la clase TipoAnimal). */
        public static List<TipoAnimal> listaDeTipos = new List<TipoAnimal>();
        
        string nombreTipo;

        public TipoAnimal( string nombreTipo)
        {

           
            this.nombreTipo = nombreTipo;
        }

       
        public string NombreTipo { get => nombreTipo; set => nombreTipo = value; }

      

        public static void leeTipos() {
            StreamReader sr = new StreamReader(@"../../Datos/Tipos.txt");
            
            string frase;
            while (!sr.EndOfStream) {
                frase = sr.ReadLine();
               

                listaDeTipos.Add(new TipoAnimal(frase));
            }
            sr.Close();
        }

        public static void Mostrar() {

            for (int i = 0; i < listaDeTipos.Count; i++)
            {
                Console.WriteLine("\t\t{0}) {1}", i+1, listaDeTipos[i].nombreTipo);
            }
           
        }
    }
}
