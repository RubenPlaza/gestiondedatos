﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p70_GestionDeDatos
{
	class Fecha
	{
		int anyo, mes, dia;

		public int Anyo { get => anyo; }
		public int Mes { get => mes;}
		public int Dia { get => dia;}

		public Fecha(int anyo, int mes, int dia)
		{
			this.anyo = anyo;
			this.mes = mes;
			this.dia = dia;
		}

		public Fecha()
		{
			this.anyo = DateTime.Now.Year;
			this.mes = DateTime.Now.Month;
			this.dia = DateTime.Now.Day;
		}

		public int FechaEntero
		{
			get
			{
				return 10000 * (anyo % 100) + 100 * mes + dia;
			}
		}

		public string FechaStringSp
		{
			get
			{
				return String.Format("{0}/{1}/{2}", dia.ToString("00"), mes.ToString("00"),anyo);
			}
		}

		public string FechaString
		{
			get
			{
				string[] tabMes = { "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
				return String.Format("{0} de {1} de {2}", dia, tabMes[mes], anyo);
			}
		}

		public bool EsBisiesto
		{
			get
			{
				return ((anyo % 4 == 0 && anyo % 100 != 0) || anyo % 400 == 0);
			}
		}

		public void CambiaFecha()
		{
			//int auxAño, auxMes, auxDia;
			int[] maxDiaMes = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
			anyo = Util.CapturaEntero("\n\tDime el año: ", 1950, 2020);
			mes = Util.CapturaEntero("\tDime el mes: ", 1, 12);
			if (mes == 2 && EsBisiesto)
				maxDiaMes[mes] = 29;
			dia = Util.CapturaEntero("\tDime el día: ", 1, maxDiaMes[mes]);
		}

		public void AvanzaDia()
		{
			//int auxAño, auxMes, auxDia;
			int[] maxDiaMes = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
			if (mes == 2 && EsBisiesto)
				maxDiaMes[mes] = 29;
			dia++;
			if (dia > maxDiaMes[mes])
			{
				dia = 1;
				mes++;
				if (mes > 12)
				{
					mes = 1;
					anyo++;
				}
			}
		}
	}
}
