﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p70_GestionDeDatos
{
    class Animal
    {
        int id;
        int idTipoAnimal;
        string nombre;
        string sexo;
        Fecha fechaNac;
        Fecha fechaInscripcion;
        //2 constructores: el primero es para los animales que se van introduciendo dentro de la aplicacion, por lo tanto su fecha de inscripcion es en el dia que se incluye;
        public Animal(int id, int idTipoAnimal, string nombre, string sexo, Fecha fechaNac)
        {
            this.id = id;
            this.idTipoAnimal = idTipoAnimal;
            this.nombre = nombre;
            this.sexo = sexo;
            this.fechaNac = fechaNac;
            this.fechaInscripcion = new Fecha();
        }
        //y el segundo es para la lectura a traves del fichero , que ya parten de una fecha de inscripcion q NO tiene porque ser HOY.
        public Animal(int id, int idTipoAnimal, string nombre, string sexo, Fecha fechaNac, Fecha fechaInscripcion)
        {
            this.id = id;
            this.idTipoAnimal = idTipoAnimal;
            this.nombre = nombre;
            this.sexo = sexo;
            this.fechaNac = fechaNac;
            this.fechaInscripcion = fechaInscripcion;
        }
        #region Propiedades
        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Sexo { get => sexo; set => sexo = value; }
        internal Fecha FechaNac { get => fechaNac; set => fechaNac = value; }
        internal Fecha FechaInscripcion { get => fechaInscripcion; set => fechaInscripcion = value; }
        public int IdTipoAnimal { get => idTipoAnimal; set => idTipoAnimal = value; }
        #endregion
        //metodo mostrar q utilizaran todas sus clases hijas
        public void Mostrar() {

            Console.WriteLine("{0} {1}{2}{3}{4}{5}", id.ToString("000"), Util.CuadraTexto( TipoAnimal.listaDeTipos[idTipoAnimal].NombreTipo,12), Util.CuadraTexto(nombre, 10), Util.CuadraTexto(sexo, 9), Util.CuadraTexto(fechaNac.FechaString, 30), fechaInscripcion.FechaStringSp);

        }
        //y un metodo abstracto para la cabezera, a la hora de filtrar por tipos de animal , cada cabezera de cada animal sera diferente.
        public void Cabezera(){}

           
        
    }
}
