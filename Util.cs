﻿using System;

namespace p70_GestionDeDatos
{
    public class Util
    {
        public static int Menu()
        {
            Console.Clear();
            Console.WriteLine("\n\n\t\t\tRubén Plaza García");
            Console.WriteLine("\t\t\t╔══════════════════════════════╗");
            Console.WriteLine("\t\t\t║        MiniZoológico         ║");
            Console.WriteLine("\t\t\t╠══════════════════════════════╣");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t║       1.- Mostrar            ║");
            Console.WriteLine("\t\t\t║       2.- Añadir             ║");
            Console.WriteLine("\t\t\t║       3.- Modificar          ║");
            Console.WriteLine("\t\t\t║       4.- Eliminar           ║");
            Console.WriteLine("\t\t\t║       5.- Filtrar            ║");
            Console.WriteLine("\t\t\t║       6.-Gestionar tipos     ║");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t║          0) Salir            ║");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t╚══════════════════════════════╝");
            Console.Write("\t\t\tElige una opcion: ");
            return (Console.ReadKey().KeyChar - 48);

        }
        public static int MenuTipos()
        {
            Console.Clear();
            Console.WriteLine("\n\n\t\t\tRubén Plaza García");
            Console.WriteLine("\t\t\t╔══════════════════════════════╗");
            Console.WriteLine("\t\t\t║        MiniZoológico         ║");
            Console.WriteLine("\t\t\t╠══════════════════════════════╣");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t║         1.- Mostrar          ║");
            Console.WriteLine("\t\t\t║         2.- Añadir           ║");
            Console.WriteLine("\t\t\t║         3.- Modificar        ║");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t║       0) Volver a Menu       ║");
            Console.WriteLine("\t\t\t║                              ║");
            Console.WriteLine("\t\t\t╚══════════════════════════════╝");
            Console.Write("\t\t\tElige una opcion: ");
            return (Console.ReadKey().KeyChar - 48);

        }
        public static char PreguntaSiNo(string texto) {
            bool esCorrecto;
            char res;

            do
            {
                
                esCorrecto = true;
                Console.Write("\n\t" + texto);
                 res = Console.ReadKey().KeyChar;

                if (res != 's' && res != 'S' && res != 'n' && res != 'N')
                {
                    esCorrecto = false;
                    Console.WriteLine("\n\tOpcion no valida");
                    Console.ReadKey();
                }

            } while (!esCorrecto);
            return res;
        }
        public static int CapturaEntero(string texto, int min, int max)
        {
            
            int num;
            bool esCorrecto;
            do
            {

                
                Console.Write("\n\t {0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out num);
                if (!esCorrecto)
                {

                    Console.WriteLine("\n\tError de FORMATO");
                    Console.ReadKey();
                }
                else if (num < min || num > max)
                {

                    Console.WriteLine("\n\tVALOR FUERA DE RANGO");
                    esCorrecto = false;
                    Console.Beep(400, 400);
                    Console.ReadKey();
                }
            } while (!esCorrecto);

            return num;
        }
        public static int CapturaEnteroModificar(string texto, int min, int max)
        {

            int num;
            string numero;
            bool esCorrecto;
            do
            {


                Console.Write("\n\t {0} [{1}..{2}]: ", texto, min, max);
                numero = Console.ReadLine();
                if (numero != string.Empty)
                {
                    esCorrecto = Int32.TryParse(numero, out num);

                    if (!esCorrecto)
                    {

                        Console.WriteLine("\n\tError de FORMATO");
                        Console.ReadKey();
                    }
                    else if (num < min || num > max)
                    {

                        Console.WriteLine("\n\tVALOR FUERA DE RANGO");
                        esCorrecto = false;
                        Console.Beep(400, 400);
                        Console.ReadKey();
                    }
                }
                else
                {
                    esCorrecto = true;
                    num = -1;
                }
            } while (!esCorrecto);

            return num;
        }

        public static int CapturaEntero(string texto)
        {

            int num;
            bool esCorrecto;
            do
            {
                
                Console.Write("\n\t {0}: ", texto);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out num);
                if (!esCorrecto)
                {

                    Console.WriteLine("\n\tError de FORMATO");
                   Console.ReadKey();
                }
                
            } while (!esCorrecto);

            return num;
        }


        public static string CapturaNombre(string texto, int numChar)
        {
            string nombre = "";
            bool ok = true;
            do
            {
                ok = true;
                Console.Write("\n\t" + texto + " [maximo:" + numChar + " caracteres]: ");
                nombre = Console.ReadLine();

                if (nombre.Length > numChar)
                {
                    Console.WriteLine("\n\t\tError* Demasiado largo");
                    ok = false;
                }
                if (nombre == string.Empty) {
                    Console.WriteLine("\n\t\tError* Nombre Vacio");
                    ok = false;
                }
            } while (!ok);
            return nombre;
        }

        //utilizo mi muestra error
        
         public static string CuadraTexto(string texto, int numChar)
        {

            texto += "                                                   ";
            return texto.Substring(0, numChar);
        }
    }
}
